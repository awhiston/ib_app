/**
 * @format
 */
import React, { Component } from 'react'
import {AppRegistry} from 'react-native';
import RootNavigator from './app/rootNavigator';
import {name as appName} from './app.json';
import ObservableStore from './app/mobx/observableStore'
import { Provider } from 'mobx-react';


class HOC extends Component {
  render () {
    return(
        <Provider observableStore={ObservableStore}>
            <RootNavigator/>
        </Provider>
    )
  }
}

AppRegistry.registerComponent("ib_app", () => HOC);
