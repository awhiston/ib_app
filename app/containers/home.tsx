import React from 'react'
import { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native'
import {observer} from 'mobx-react'

export default class Home extends React.Component {
  static navigationOptions = {
    title: 'Home',
  };
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }

  addName = () => {
    this.props.screenProps.observableStore.addUserName(this.state.text)

    this.props.navigation.navigate('Welcome')
  }



  render() {
    return (

      <View style={styles.container}>
        <Text style={styles.h1}>Welcome, friend</Text>
        <Text style={styles.h2}>What's your name?</Text>

        <TextInput
          style={styles.textInput}
          onChangeText={(text) => this.setState({text})}
          value={this.state.text}
          autoFocus={true}
        />


        <TouchableOpacity onPress={this.addName} style={styles.button}>
            <Text style={styles.buttonText}>
                That's me!
            </Text>
        </TouchableOpacity>

      </View>
    )
  }
}


const styles = StyleSheet.create({
    container:{
      flex:1,
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'flex-start'
    },
          h1:{
            fontSize:22,
            marginTop:15,
            margin:5
          },
          h2:{
            fontSize:16,
            margin:5
          },
          textInput:{
            width:'80%',
            backgroundColor:'grey',
            margin:20,
            marginTop:30,
            fontSize:24,
            color:'white',
            textAlign:'center'
          },
          button:{
            padding:10,
            backgroundColor:'purple'
          },
                buttonText:{
                  color:'white',
                  fontSize:20
                }
});
