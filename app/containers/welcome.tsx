import React from 'react'
import { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native'
import {observer} from 'mobx-react'

export default class Welcome extends React.Component {
  static navigationOptions = {
    title: 'Welcome',
  };
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
    this.compliments = [
      'beautiful',
      'glorious',
      'strange',
      'peculiar',
      'charming',
      'bizarre',
      'stupendous',
      'contentious',
      'barbaric',
      'delightful'
    ]
  }

  componentWillMount = () =>{
    this.setState({
      compliment: this.compliments[Math.floor(Math.random() * this.compliments.length)]
    })
  }

  render() {
    const store = this.props.screenProps.observableStore;
    return (

      <View style={styles.container}>

        {store.getUserName ?
          <Text style={styles.h1}>{store.getUserName}!?!</Text>
        :
          <Text style={styles.h1}>Hmm, that ain't no name</Text>
        }

        {store.getUserName!=='' &&
          <Text style={styles.h2}>What a <Text style={{fontWeight: "bold"}}>{[this.state.compliment]}</Text> name.</Text>
        }

        <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={styles.button}>
            <Text style={styles.buttonText}>
                {store.getUserName!==''?
                  "Actually, that's not me!"
                :
                  "Ok, I'll play fair"
                }
            </Text>
        </TouchableOpacity>

      </View>
    )
  }
}


const styles = StyleSheet.create({
    container:{
      flex:1,
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'flex-start'
    },
          h1:{
            fontSize:26,
            marginTop:15,
            margin:5,
            fontWeight:'bold'
          },
          h2:{
            fontSize:16,
            margin:5,
            marginBottom:15,
          },
          textInput:{
            width:'80%',
            backgroundColor:'grey',
            margin:20,
            marginTop:30
          },
          button:{
            padding:10,
            backgroundColor:'red',
            margin:20
          },
                buttonText:{
                  color:'white',
                  fontSize:20
                }
});
