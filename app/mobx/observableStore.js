import {observable, computed} from 'mobx'

class Store {
  @observable userName = ''

  @computed get getUserName() {
    return this.userName.toUpperCase();
  }

  addUserName (userName: string) {
    this.userName = userName
  }

  removeUserName () {
    this.userName = ''
  }

}

const ObservableStore = new Store()
export default ObservableStore
