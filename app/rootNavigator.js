import { createStackNavigator, createAppContainer } from 'react-navigation'
import {inject} from 'mobx-react'

import Home from './containers/home'
import Welcome from './containers/welcome'

const RootNavigator = createStackNavigator(
  {
    Home: {screen: Home},
    Welcome: {screen: Welcome},
  },
  {
    initialRouteName: "Home"
  }
)

const RouterWithStore = inject(store => {
  return {
    screenProps: {
      ...store,
    },
  }
})(RootNavigator);

export default createAppContainer(RouterWithStore);
